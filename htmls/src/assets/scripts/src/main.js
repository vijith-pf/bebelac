$(document).ready(function() {

  // Validate Form
  $.validator.addMethod("namefield", function(value, element) { return this.optional(element) || /^[a-z\'\s]+$/i.test(value); }, "Invalid Characters");
  $.validator.addMethod("phonefield", function(value, element) { return this.optional(element) || /^[0-9\+\s]+$/i.test(value); }, "Invalid phone number");
  $.validator.addMethod("msgfield", function(value, element) { return this.optional(element) || /^[a-z\0-9\,.'"!()+@\s]+$/i.test(value); }, "Invalid Characters");

  $("#application").validate({
    rules: {
      name: {
        required: true,
        minlength: 2,
        maxlength: 25,
        namefield: true
      },
      email: {
        required: true,
        maxlength: 35,
        email: true
      },
      phone: {
        required: true,
        phonefield: true,
        minlength: 10,
        maxlength: 25,
      },
      productOptions: {
        required: function() {
          return $('[name="productOptions"]:checked').length === 0;
        }
      }
    },
    messages: {
      userGender: {
        required: "please select an option"
      },
    },
    submitHandler: function(form) {
      // some other code
      // maybe disabling submit button
      // then:
      //alert("submitted!");
      $('.apply-form .detail-box .btn').addClass('disabled');
      $(form).submit();
    }
  });


  $('.form-control').placeholder();


  var $bannerSlider = $('.banner-slideshow .slider').on('init', function(slick) {

    $('.banner-slideshow').show();

  }).slick({

    lazyLoad: 'ondemand',
    autoplay: true,
    autoplaySpeed: 5000,
    dots: false,
    arrows: true,
    centerMode: false,
    infinite: true,
    speed: 800,
    fade: false,
    pauseOnHover: false,
    pauseOnFocus: false,
    prevArrow: $('.banner-prev'),
    nextArrow: $('.banner-next')

  });

  // Custom Select
  $('selectpicker').select2();

  //Collapsing panel
  $('.collapsing-panel .panel-head h6').prepend('<i class="icomoon icon-toggle"></i> ');
  $('.collapsing-panel .panel-head').click(function(e) {

    $('.panel-details').slideUp();
    $(this).next('.panel-details').slideDown();

    e.preventDefault();
  });


  $('.num-selector input').number({
    'containerClass' : 'number-style',
    'minus' : 'number-minus',
    'plus' : 'number-plus',
    'containerTag' : 'div',
    'btnTag' : 'span'
  });

});

function peiCharts(){
  $('.chart-vitamin-d').easyPieChart({
    barColor: '#AFEFE3',
    scaleColor: false,
    lineWidth: 13,
    lineCap: 'circle',
    size: 120,
    trackColor: '#36AE97',
    scaleLength: 0,
    onStep: function(from, to, percent) {
      $(this.el).find('span').text(Math.round(percent)+ '%');
    }
  });
  $('.chart-calcium').easyPieChart({
    barColor: '#E0EE95',
    scaleColor: false,
    lineWidth: 13,
    lineCap: 'circle',
    size: 120,
    trackColor: '#36AE97',
    scaleLength: 0,
    onStep: function(from, to, percent) {
      $(this.el).find('span').text(Math.round(percent)+ '%');
    }
  });
  $('.chart-vitamin-c').easyPieChart({
    barColor: '#FCDD4C',
    scaleColor: false,
    lineWidth: 13,
    lineCap: 'circle',
    size: 120,
    trackColor: '#36AE97',
    scaleLength: 0,
    onStep: function(from, to, percent) {
      $(this.el).find('span').text(Math.round(percent)+ '%');
    }
  });
}

if($('#thing').length > 0){
var waypoint = new Waypoint({
  element: document.getElementById('thing'),
  handler: function(direction) {
    peiCharts();  
  }
});
}

peiCharts();

$(window).on("load",function(){

  var itemsCount = $('.wrapper-graph-items').length;
  $('.graph-inner-wrap').css({'opacity': 1});

  if($(this).width()>991){

      $('.tower-progress').each(function(){
                  var iornVal = $(this).attr('data-mg');
                  iornCalcVal = iornVal * 25.2;
                  if(iornCalcVal > 325){
                    $(this).css({'padding-top': 325});
                  } else {
                    $(this).css({'padding-top': iornCalcVal});
                  }
                });

                $('.wrapper-graph-items .iron-progress').each(function(){
                  var iornVal = $(this).attr('data-mg');
                  iornCalcVal = iornVal * 25.2;
                  if(iornCalcVal > 325){
                    $(this).css({'padding-top': 300});
                  } else {
                    $(this).css({'padding-top': iornCalcVal});
                  }
                });

        $(".iron-data-scroll").mCustomScrollbar({
          axis:"x",
          advanced:{autoExpandHorizontalScroll:true},
          callbacks:{
            onInit:function(){
              $('.graph-inner-wrap').css({'opacity': 1});
              setTimeout(function(){

                $('.tower-progress').each(function(){
                  var iornVal = $(this).attr('data-mg');
                  iornCalcVal = iornVal * 25.2;
                  if(iornCalcVal > 325){
                    $(this).css({'padding-top': 325});
                  } else {
                    $(this).css({'padding-top': iornCalcVal});
                  }
                });

                $('.wrapper-graph-items .iron-progress').each(function(){
                  var iornVal = $(this).attr('data-mg');
                  iornCalcVal = iornVal * 25.2;
                  if(iornCalcVal > 325){
                    $(this).css({'padding-top': 300});
                  } else {
                    $(this).css({'padding-top': iornCalcVal});
                  }
                });

              }, 300);
            }
          }
        });
      } else {
        $(".iron-data-scroll").mCustomScrollbar("destroy"); //destroy scrollbar 

        var totalwidth = $('.graph-line-wrap').width();
        var sectionWidth = totalwidth / 14;

        $('.tower-progress').each(function(){
          var iornVal = $(this).attr('data-mg');
          iornCalcVal = sectionWidth * iornVal;
          $(this).css({'width': iornCalcVal + sectionWidth});
        });

        $('.wrapper-graph-items .iron-progress').each(function(){
          var iornVal = $(this).attr('data-mg');
          iornCalcVal = sectionWidth * iornVal;
          $(this).css({'width': iornCalcVal + sectionWidth});
        });

      }

 $(window).resize(function(){
      if($(this).width()>991){
        $(".iron-data-scroll").mCustomScrollbar({
          axis:"x",
          advanced:{autoExpandHorizontalScroll:true},
          callbacks:{
            onInit:function(){
              $('.graph-inner-wrap').css({'opacity': 1});
              setTimeout(function(){

                $('.tower-progress').each(function(){
                  var iornVal = $(this).attr('data-mg');
                  iornCalcVal = iornVal * 25.2;
                  if(iornCalcVal > 325){
                    $(this).css({'padding-top': 325});
                  } else {
                    $(this).css({'padding-top': iornCalcVal});
                  }
                });

                $('.wrapper-graph-items .iron-progress').each(function(){
                  var iornVal = $(this).attr('data-mg');
                  iornCalcVal = iornVal * 25.2;
                  if(iornCalcVal > 325){
                    $(this).css({'padding-top': 300});
                  } else {
                    $(this).css({'padding-top': iornCalcVal});
                  }
                });

              }, 300);
            }
          }
        });
      } else {
        $(".iron-data-scroll").mCustomScrollbar("destroy"); //destroy scrollbar 

        var totalwidth = $('.graph-line-wrap').width();
        var sectionWidth = totalwidth / 14;

        $('.tower-progress').each(function(){
          var iornVal = $(this).attr('data-mg');
          iornCalcVal = sectionWidth * iornVal;
          $(this).css({'width': iornCalcVal + sectionWidth});
        });

        $('.wrapper-graph-items .iron-progress').each(function(){
          var iornVal = $(this).attr('data-mg');
          iornCalcVal = sectionWidth * iornVal;
          $(this).css({'width': iornCalcVal + sectionWidth});
        });

      }
  }).trigger("resize");




 $(window).resize(function(){
      if($(this).width()>991){
        $(".recipes-rich-scroll").mCustomScrollbar({
          axis:"x",
          advanced:{autoExpandHorizontalScroll:true},
          callbacks:{
          }
        });
      } else {
        $(".recipes-rich-scroll").mCustomScrollbar("destroy"); //destroy scrollbar 
      }
  }).trigger("resize");
});