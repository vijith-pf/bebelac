<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'bebelac');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'gi  !LLGFTJ8r<<s`(fe*4]bv+rXig*3ud](hrI9E>y/YqS3vdKf$cwf3!4Vo7*a');
define('SECURE_AUTH_KEY',  'Pl!AdUFM,}r2SiJowmcqi##?*BySEq|>$A9lQ}.}(.;8AU!N|9rM.Ii?#9T@6?Bs');
define('LOGGED_IN_KEY',    'd+GDQ!S$|4{Y*FDtQ/rp*{Z;_OgH2^$xwf),eBP2NjrR$M?(jDsm-qyMOaxLh^$g');
define('NONCE_KEY',        '/BR^_+ru@ x{fh`6^Ns2l{Z.?~e6`F2S0AH9GyQ$Ls]PSY^uY3=i9H,6r}&iutU^');
define('AUTH_SALT',        'GSgJ>0C8M^uOC#s(w,MB.nje2^=1Q)wM@M.Yb64umFJ>@49ZQ}o3&&0-`(,k6;cI');
define('SECURE_AUTH_SALT', 'YBdFtd+Jp0Iy<MYDi_c3i3F4/|u+us~w= 6DP+(BZ6qy!peNG0?SpLIG`^!CXj;q');
define('LOGGED_IN_SALT',   '|CH_3ViJ|OGjb$D+$1S(t[OS7(jt`2D=Os;DcWbv>G};(]};3z`+>Z e5$wIlyx/');
define('NONCE_SALT',       'e&PF)`HdhV]`e5~QdZJO?< LUV0qW(#2x|s%_c4}>>B>`;wDZ[I+j^7W50[lB]vW');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'bebelac_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
