<section>

	<div class="container" style="text-align: center; padding: 50px 0px;">

	<?php get_template_part('templates/page', 'header'); ?>

	<div class="alert alert-warning">
	  <?php _e('Sorry, but the page you were trying to view does not exist.', 'roots'); ?>
	</div>

	<a href="<?php echo home_url(); ?>/" class="btn btn-link">Back to Home Page</a>

	</div>


</section>