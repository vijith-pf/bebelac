<?php get_template_part('templates/head'); ?>
<body <?php body_class(); ?>>

  <!--[if lt IE 8]>
    <div class="alert alert-warning">
      <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'roots'); ?>
    </div>
  <![endif]-->

  <?php
    do_action('get_header');
    get_template_part('templates/header');
  ?>

  <?php if (is_front_page()) { ?>
    <?php get_template_part('templates/home', 'page'); ?>
  <?php } else { ?>
    <?php include roots_template_path(); ?>
  <?php } ?>

  <?php get_template_part('templates/footer'); ?>

</body>
</html>
