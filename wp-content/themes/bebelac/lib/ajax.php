<?php 
//Ajax Functions

add_action('wp_ajax_selecttedList','selecttedList');
add_action('wp_ajax_nopriv_selecttedList','selecttedList');
function selecttedList(){
	$postIds = $_POST['postIds'];
	foreach ($postIds as $post_count) {
		$post = get_post( $post_count['postid'] );
	    ?>
    	<div class="nutrition-item" data-mh="nutrition">
            <div class="oval">
              <div class="img-wrap">
                <?php
                if(has_post_thumbnail($post->ID)):
                echo get_the_post_thumbnail($post->ID,'meals-thumb-small');
                else:
                echo '<img src="'.get_template_directory_uri().'/assets/images/default-img.png" alt="'.$post->post_title.'" title="'.get_the_title().'" />';
                endif;
                ?>
              </div>
              <h4><?php echo $post->post_title; ?></h4>
            </div>
            <h3><?php echo $post->post_content; ?></h3>
            <div class="num-selector">
              <input type="number" value="<?php echo $post_count['count_items']; ?>" step="1" min="0" max="10" id="<?php echo $post->ID; ?>" name="<?php echo $post->ID; ?>">
            </div>
      	</div>	
    	<?php
	}
	die();

}
