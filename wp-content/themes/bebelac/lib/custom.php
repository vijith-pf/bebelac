<?php
/**
* Custom functions
*/

if ( function_exists( 'add_theme_support' ) ) { 
     add_theme_support( 'post-thumbnails' );
     set_post_thumbnail_size( 300, 324, true ); // default Post Thumbnail dimensions (cropped)
     add_image_size( 'meals-thumb', 150, 150, true );
     add_image_size( 'meals-thumb-small', 100, 100, true );
}

wp_enqueue_style( 'dashicons' );

// Highlihgt Custom posttype Menu item
function remove_parent_classes($class)
{
	return ($class == 'active' || $class == 'active' || $class == 'active') ? FALSE : TRUE;
}
function add_class_to_wp_nav_menu($classes)
{
     switch (get_post_type())
     {
     	case 'units':
     		// we're viewing a custom post type, so remove the 'current_page_xxx and current-menu-item' from all menu items.
     		$classes = array_filter($classes, "remove_parent_classes");

     		// add the current page class to a specific menu item (replace ###).
     		if (in_array('menu-business-units', $classes))
     		{
				$classes[] = 'active';
			}
     		break;
     }
	return $classes;
}
add_filter('nav_menu_css_class', 'add_class_to_wp_nav_menu');


function get_all_get(){
        $output = "?"; 
        $firstRun = true; 
        foreach($_GET as $key=>$val) { 
        if($key != $parameter) { 
            if(!$firstRun) { 
                $output .= "&"; 
            } else { 
                $firstRun = false; 
            } 
            $output .= $key."=".$val;
         } 
    } 

    return $output;
} 