<?php
function revcon_change_post_label() {
    global $menu;
    global $submenu;
    $menu[5][0] = 'News & Events';
    $submenu['edit.php'][5][0] = 'News & Events';
    $submenu['edit.php'][10][0] = 'Add New';
    $submenu['edit.php'][16][0] = 'News Tags';
    echo '';
}
function revcon_change_post_object() {
    global $wp_post_types;
    $labels = &$wp_post_types['post']->labels;
    $labels->name = 'News & Events';
    $labels->singular_name = 'News & Events';
    $labels->add_new = 'Add New';
    $labels->add_new_item = 'Add New';
    $labels->edit_item = 'Edit News & Events';
    $labels->new_item = 'News & Events';
    $labels->view_item = 'View News & Events';
    $labels->search_items = 'Search News & Events';
    $labels->not_found = 'No News & Events found';
    $labels->not_found_in_trash = 'No News & Events found in Trash';
    $labels->all_items = 'All News & Events';
    $labels->menu_name = 'News & Events';
    $labels->name_admin_bar = 'News & Events';
}
 
add_action( 'admin_menu', 'revcon_change_post_label' );
add_action( 'init', 'revcon_change_post_object' );