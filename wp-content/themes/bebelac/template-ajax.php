<?php
/*
Template Name: Ajax Results Template
*/
?>

<?php get_template_part('templates/page', 'header'); ?>
<?php // get_template_part('templates/content', 'page'); ?>

<form class="form-inline">
  <div class="form-group">
    <input type="search" class="form-control" name="searchData" id="searchData" placeholder="Search Keyword" value=""/>
  </div>
  <div class="form-group">
    <select class="form-control" name="filterTopic" id="filterTopic">
		<option value="-1" disabled="disabled">Select Topic</option>
		<?php 
		$terms = get_terms( 'topics' );
	 	if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){
	     	foreach ( $terms as $term ) {
	       		echo '<option class="level-0" value="'.$term->term_id.'">'.$term->name.'</option>';
	    	}
		}
		?>
	</select>
  </div>
  <div class="form-group">
	<select id="order_by" name="order_by" class="form-control">
		<option value="ASC">ASC</option>
		<option value="DESC">DESC</option>
	</select>
  </div>
  <button type="submit" class="btn btn-primary" id="filterResults" name="filterResults">Filter</button>
</form>



<br/>
<div class="" id="ajaxData">
	Content Loads here
</div>
<br/>
<button class="btn btn-primary" onClick="fetchData()">Load All Posts</button>
<br/>&nbsp;<br/>

<script type="text/javascript">
	var ajaxurl = '<?php echo admin_url('admin-ajax.php') ?>';
	function fetchData(){
		$.post(ajaxurl, {'action':'my_action'}, function(response){
			//console.log(response);
			$('#ajaxData').empty().append(response);
		});
	}
	function filterData(){
		$('#ajaxData').prepend('<div class="load-spinner">Loading...</div>');
		var searchVal = $('#searchData').val();
		var filterTopic = $('#filterTopic').val();
		var orderList = $('#order_by').val();
	  	$.post(ajaxurl, {'action':'search_result', 'searchFor': searchVal, 'showcategory': filterTopic, 'order_by': orderList}, function(response){
			//console.log(response);
			$('#ajaxData').empty().append(response);
			$('#ajaxData .load-spinner').remove();
		});
	}
	$( "#searchData" ).keyup(function(event) {
		$('#ajaxData .load-spinner').remove();
		event.preventDefault();
		filterData();
	});

	$( "#filterResults" ).click(function(event) {
		event.preventDefault();
		filterData();
	});

	$( "#filterTopic, #order_by" ).change(function(event) {
		event.preventDefault();
		filterData();
	});
</script>
