<?php
/*
Template Name: Iron Test Result Template
*/
?>

<?php // get_template_part('templates/page', 'header'); ?>
<?php // get_template_part('templates/content', 'page'); ?>

<section class="hero-inner-wrap">
<div class="container">
  <div class="hero-inner-container">
    <div class="static-wrap">
      <div class="word-wrap">
        <div class="word-inner">
          <div class="line-txt">
            <span class="firstline">The</span>
            <span class="secondline">Bebelac</span>
            <span class="thirdline">Junior</span>
          </div>
          <div class="bebelac-count">3</div>
        </div>
        <h1>Iron Test</h1>
      </div>
      <div class="img-wrap">
        <img src="<?php echo get_template_directory_uri(); ?>/contents/strong-boy.png" alt="" />
      </div>
    </div>

    <?php
    $sum = 0;
    foreach($_GET as $key=>$value){
      //echo $key;
      //echo $value;
      $post = get_post( $key );
      $sum += get_field('food_iron_content', $post->ID) * $value;
    }
    ?>

    <?php if($sum): ?>
    <div class="result info-right">
      <div class="result-wrap">
        <div class="result-wrap-inner">
          <h2>Your<br/>Result</h2>
          <div class="oval">
            <h3><?php echo number_format($sum, 0); ?></h3>
            <h5>MG</h5>
          </div>
        </div>
        <h3>Recommended Iron intake is 9 MG</h3>
      </div>
    </div>
    <?php endif; ?>


  </div>
</div>
</section>

<?php if($sum > 8): ?>
<section class="result-info">
<div class="container">
  <div class="result-wrap">
    <div class="message-wrap">
      <div class="title">
        <h3>Your baby seems to be consuming enough iron to meet his needs.</h3>
      </div>
      <p>You can fight iron deficiency with the support of Bebelac junior 3 with PreciNutri™ formulated for toddlers between ages one and three. It provides easily digested, crucial nutrients at the levels required for this age.</p>
    </div>
    <div class="info">
      <div class="title">
        <h4>Two feeds of Bebelac Junior 3 provide 2/3 of your child’s daily nutritional intake of Iron. </h3>
      </div>
      <div class="info-flow">
        <div class="bebelac-bottle">
          <div class="img-wrap" data-mh="img-wrap">
            <img src="<?php echo get_template_directory_uri(); ?>/contents/bebebot.png" alt="" />
          </div>
          <h3>2 FEEDS OF BEBELAC JUNIOR 3<br/>(500 ml)</h3>
        </div>
        <div class="diagram-holder">
          <div class="img-wrap" data-mh="img-wrap">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/dialy.svg" alt="" />
          </div>
          <h3>DAILY IRON INTAKE</h3>
        </div>
      </div>
    </div>
  </div>
  <div class="btn-wrap center">
    <a href="#" class="btn btn-md btn-primary">Download Result</a>
  </div>
</div>
</section>
<?php else: ?>
<section class="result-info">
<div class="container">
  <div class="result-wrap">
    <div class="message-wrap">
      <div class="title">
        <h3>You should Increase intake of food rich in Iron. Start with providing the toddler with iron-rich foods every day. Usually, the iron existing in red meat and fish can be easily absorbed by the body.</h3>
      </div>
      <p>Other foods rich in Iron include: broccoli, spinach, liver, peas, chicken, turkey, lamb, grains and cereals.</p>
      <br/>
      <p>You can fight iron deficiency with the support of Bebelac Junior 3 with PreciNutri™ formulated for toddlers between ages one and three. It provides easily digested, crucial nutrients at the levelsrequired for this age.</p>
    </div>
    <div class="info">
      <div class="title">
        <h4>Two feeds of Bebelac Junior 3 provide 2/3 of your child’s daily nutritional intake of Iron. </h3>
      </div>
      <div class="info-flow">
        <div class="bebelac-bottle">
          <div class="img-wrap" data-mh="img-wrap">
            <img src="<?php echo get_template_directory_uri(); ?>/contents/bebebot.png" alt="" />
          </div>
          <h3>2 FEEDS OF BEBELAC JUNIOR 3<br/>(500 ml)</h3>
        </div>
        <div class="diagram-holder">
          <div class="img-wrap" data-mh="img-wrap">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/dialy.svg" alt="" />
          </div>
          <h3>DAILY IRON INTAKE</h3>
        </div>
      </div>
    </div>
  </div>
  <div class="btn-wrap center">
    <a href="#" class="btn btn-md btn-primary">Download Result</a>
  </div>
</div>
</section>
<?php endif; ?>