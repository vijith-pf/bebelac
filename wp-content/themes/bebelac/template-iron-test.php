<?php
/*
Template Name: Iron Test Template
*/
?>

<?php // get_template_part('templates/page', 'header'); ?>
<?php // get_template_part('templates/content', 'page'); ?>

<section class="hero-inner-wrap">
<div class="container">
  <div class="hero-inner-container">
    <div class="static-wrap">
      <div class="word-wrap">
        <div class="word-inner">
          <div class="line-txt">
            <span class="firstline">The</span>
            <span class="secondline">Bebelac</span>
            <span class="thirdline">Junior</span>
          </div>
          <div class="bebelac-count">3</div>
        </div>
        <h1>Iron Test</h1>
      </div>
      <div class="img-wrap">
        <img src="<?php echo get_template_directory_uri(); ?>/contents/strong-boy.png" alt="" />
      </div>
    </div>
    <div class="take-food info-right">
      <h3>How to take the iron test?</h3>
      <div class="steps-wrap">
        <div class="steps">
          <span class="icon-wrap">
            <i class="icon icon-food"></i>
          </span>
          <div class="step-count">01</div>
          <p>Choose the foods consumed by your child or closest ingredient</p>
        </div>
        <div class="steps">
          <span class="icon-wrap">
            <i class="icon icon-portion"></i>
          </span>
          <div class="step-count">02</div>
          <p>Choose the portion you feed daily</p>
        </div>
        <div class="steps">
          <span class="icon-wrap">
            <i class="icon icon-meals"></i>
          </span>
          <div class="step-count">03</div>
          <p>Choose the portion you feed daily</p>
        </div>
        <div class="steps">
          <span class="icon-wrap">
            <i class="icon icon-result"></i>
          </span>
          <div class="step-count">04</div>
          <p>Once done, click calculate</p>
        </div>
      </div>
    </div>
  </div>
</div>
</section>

<section class="tab-holder">
<div class="container">
  
  <div class="panel-switch-wrap">
    <div class="panel-switch">
      <a href="#breakfast" class="btn btn-switch bg-orange active" data-opentab>Breakfast</a>
      <a href="#lunch" class="btn btn-switch bg-green" data-opentab>Lunch</a>
      <a href="#snacks" class="btn btn-switch bg-blue" data-opentab>Snacks</a>
      <a href="#dinner" class="btn btn-switch bg-red" data-opentab>Dinner</a>
    </div>
  </div>

  <div class="panel-collapse bg-orange active">
    <a href="#breakfast" class="btn btn-switch-mobile bg-orange active" data-opentab>Breakfast</a>

    <?php
    $args = array(
      'post_type' => 'food',
      'posts_per_page' => -1,  //show all posts
      'tax_query' => array(
          array(
              'taxonomy' => 'food_cat',
              'field' => 'slug',
              'terms' => 'breakfast',
          )
      )
    );
    $posts = new WP_Query($args);
    if( $posts->have_posts() ):
    ?>
    <div class="panel active" id="breakfast" data-opentab>
      <div class="panel-inner">
        <div class="nutrition-wrap"> 
          <?php while( $posts->have_posts() ) : $posts->the_post(); ?>
          <div class="nutrition-item" data-mh="nutrition">
            <div class="oval">
              <div class="img-wrap">
                <?php
                if(has_post_thumbnail()):
                the_post_thumbnail('meals-thumb-small');
                else:
                echo '<img src="'.get_template_directory_uri().'/assets/images/default-img.png" alt="'.get_the_title().'" title="'.get_the_title().'" />';
                endif;
                ?>
              </div>
              <h4><?php echo get_the_title(); ?></h4>
            </div>
            <h3><?php echo the_content(); ?></h3>
            <div class="num-selector">
              <input type="number" value="0" step="1" min="0" max="10" id="<?php echo get_the_ID(); ?>" name="<?php echo basename(get_permalink()); ?>">
            </div>
          </div>
          <?php
          endwhile;
          wp_reset_query();
          ?>
        </div>
      </div>
      <div class="panel-footer">
        <!--<a href="#" class="link prev prev-arrow-icon">Back</a>-->
        <a href="#lunch" class="link next next-arrow-icon end" data-opentab >Next, Select from Lunch</a>
      </div>
    </div>
  </div>
  <?php endif; ?>


  <?php
  $args = array(
    'post_type' => 'food',
    'posts_per_page' => -1,  //show all posts
    'tax_query' => array(
        array(
            'taxonomy' => 'food_cat',
            'field' => 'slug',
            'terms' => 'lunch',
        )
    )
  );
  $posts = new WP_Query($args);
  if( $posts->have_posts() ):
  ?>
  <div class="panel-collapse bg-green">
    <a href="#lunch" class="btn btn-switch-mobile bg-green" data-opentab>Lunch</a>
    <div class="panel" id="lunch" data-opentab>
      <div class="panel-inner">
        <div class="nutrition-wrap"> 
          <?php while( $posts->have_posts() ) : $posts->the_post(); ?>
          <div class="nutrition-item" data-mh="nutrition">
            <div class="oval">
              <div class="img-wrap">
                <?php
                if(has_post_thumbnail()):
                the_post_thumbnail('meals-thumb-small');
                else:
                echo '<img src="'.get_template_directory_uri().'/assets/images/default-img.png" alt="'.get_the_title().'" title="'.get_the_title().'" />';
                endif;
                ?>
              </div>
              <h4><?php echo get_the_title(); ?></h4>
            </div>
            <h3><?php echo the_content(); ?></h3>
            <div class="num-selector">
              <input type="number" value="0" step="1" min="0" max="10" id="<?php echo get_the_ID(); ?>" name="<?php echo basename(get_permalink()); ?>">
            </div>
          </div>
          <?php
          endwhile;
          wp_reset_query();
          ?>
        </div>
      </div>
      <div class="panel-footer">
        <a href="#breakfast" class="link prev prev-arrow-icon" data-opentab>Back</a>
        <a href="#snacks" class="link next next-arrow-icon" data-opentab>Next, Select from Snacks</a>
      </div>
    </div>
  </div>
  <?php endif; ?>

  <?php
  $args = array(
    'post_type' => 'food',
    'posts_per_page' => -1,  //show all posts
    'tax_query' => array(
        array(
            'taxonomy' => 'food_cat',
            'field' => 'slug',
            'terms' => 'snack',
        )
    )
  );
  $posts = new WP_Query($args);
  if( $posts->have_posts() ):
  ?>
  <div class="panel-collapse bg-blue">
    <a href="#snacks" class="btn btn-switch-mobile bg-blue" data-opentab>Snacks</a>
    <div class="panel" id="snacks" data-opentab>      
      <div class="panel-inner">
        <div class="nutrition-wrap"> 
          <?php while( $posts->have_posts() ) : $posts->the_post(); ?>
          <div class="nutrition-item" data-mh="nutrition">
            <div class="oval">
              <div class="img-wrap">
                <?php
                if(has_post_thumbnail()):
                the_post_thumbnail('meals-thumb-small');
                else:
                echo '<img src="'.get_template_directory_uri().'/assets/images/default-img.png" alt="'.get_the_title().'" title="'.get_the_title().'" />';
                endif;
                ?>
              </div>
              <h4><?php echo get_the_title(); ?></h4>
            </div>
            <h3><?php echo the_content(); ?></h3>
            <div class="num-selector">
              <input type="number" value="0" step="1" min="0" max="10" id="<?php echo get_the_ID(); ?>" name="<?php echo basename(get_permalink()); ?>">
            </div>
          </div>
          <?php
          endwhile;
          wp_reset_query();
          ?>
        </div>
      </div>
      <div class="panel-footer">
        <a href="#lunch" class="link prev prev-arrow-icon" data-opentab>Back</a>
        <a href="#dinner" class="link next next-arrow-icon" data-opentab>Next, Select from Dinner</a>
      </div>
    </div>
  </div>
  <?php endif; ?>


  <?php
  $args = array(
    'post_type' => 'food',
    'posts_per_page' => -1,  //show all posts
    'tax_query' => array(
        array(
            'taxonomy' => 'food_cat',
            'field' => 'slug',
            'terms' => 'dinner',
        )
    )
  );
  $posts = new WP_Query($args);
  if( $posts->have_posts() ):
  ?>
  <div class="panel-collapse bg-red">
    <a href="#dinner" class="btn btn-switch-mobile bg-red" data-opentab>Dinner</a>
    <div class="panel" id="dinner" data-opentab> 
      <div class="panel-inner">
        <div class="nutrition-wrap"> 
          <?php while( $posts->have_posts() ) : $posts->the_post(); ?>
          <div class="nutrition-item" data-mh="nutrition">
            <div class="oval">
              <div class="img-wrap">
                <?php
                if(has_post_thumbnail()):
                the_post_thumbnail('meals-thumb-small');
                else:
                echo '<img src="'.get_template_directory_uri().'/assets/images/default-img.png" alt="'.get_the_title().'" title="'.get_the_title().'" />';
                endif;
                ?>
              </div>
              <h4><?php echo get_the_title(); ?></h4>
            </div>
            <h3><?php echo the_content(); ?></h3>
            <div class="num-selector">
              <input type="number" value="0" step="1" min="0" max="10" id="<?php echo get_the_ID(); ?>" name="<?php echo basename(get_permalink()); ?>">
            </div>
          </div>
          <?php
          endwhile;
          wp_reset_query();
          ?>
        </div>
      </div>
      <div class="panel-footer">
        <a href="#snacks" class="link prev prev-arrow-icon" data-opentab>Back</a>
        <!--
        <a href="#selected_items" class="link next next-arrow-icon preview" data-opentab>Preview</a>
        <button class="link next next-arrow-icon end">Preview</button>
        -->
      </div>
    </div>
  </div>
  <?php endif; ?>

  <div class="sticky-button preview-btn">
    <a href="#selected_items" class="btn btn-sticky" data-opentab>Preview <i class="icon icon-arrow"></i></a>
  </div>

  <form action="<?php echo home_url(); ?>/iron-test-result">
    <div class="panel bg-primary_light" id="selected_items" data-opentab>
      <div class="panel-header">
        <h2>SELECTED ITEMS</h2>
        <p>You can either change the number of portions or remove items from this selected list.</p>
      </div>
      <div class="panel-inner">
        <div class="nutrition-wrap" id="ajaxData"></div>
      </div>
      <div class="panel-footer">
        <a href="#dinner" class="link prev prev-arrow-icon back-to-selector" data-opentab>Back</a>
        <!--<button class="link next next-arrow-icon">Calculate</button>-->
      </div>
    </div>
    <div class="sticky-button calculate-btn">
      <button class="btn btn-sticky" data-opentab>Calculate <i class="icon icon-arrow"></i></button>
    </div>
  </form>

</div>
</section>
