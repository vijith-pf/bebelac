<footer class="bg-yellow">
	<div class="container">
	  <div class="footer-inner">
	    <div class="social-media">
	      <div class="label-txt">FOLLOW US ON</div>
	      <div class="icon-wrap">
	        <a href="#">
	          <i class="icon icon-facebook"></i>
	        </a>
	      </div>
	    </div>
	    <div class="footer-menu">
	      <ul class="nav-items">
	        <li><a href="#">CONTACT US</a></li>
	        <li><a href="#">TERMS & CONDITIONS</a></li>
	        <li><a href="#">PRIVACY POLICY</a></li>
	      </ul>
	    </div>
	    <div class="copyright">
	      &copy; <?php bloginfo('name'); ?> <?php echo date('Y'); ?>
	    </div>
	  </div>
	</div>
</footer>

<?php wp_footer(); ?>

<style type="text/css">
	.load-spinner{
		width: 100%;
		display: block;
	}
</style>

<script type="text/javascript">
var ajaxurl = '<?php echo admin_url('admin-ajax.php') ?>';
function filterData(){
	//$('#ajaxData').prepend('<div class="load-spinner">Loading...</div>');
	var postIds = [];
	$('.panel-collapse .num-selector input').each(function(){
		var inputal  = $(this).val();
		var inputid  = $(this).attr('id');
		if(inputal!=0){
			postIds.push({
	            count_items: inputal, 
	            postid:  inputid
	        });
		}
	});
  	$.post(ajaxurl, {'action':'selecttedList', 'postIds': postIds}, function(response,result){
		
		$('#ajaxData').empty().append(response);
		//$('#ajaxData .load-spinner').remove();

  		$('#ajaxData .num-selector input').number({
		    'containerClass' : 'number-style',
		    'minus' : 'number-minus',
		    'plus' : 'number-plus',
		    'containerTag' : 'div',
		    'btnTag' : 'span'
	    });

  		$('.calculate-btn').show();

		
	});
}
$( ".preview-btn" ).click(function(event) {
	event.preventDefault();
	filterData();
});
</script>

<!-- Google Analytics -->
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-XXXXX-Y', 'auto');
ga('send', 'pageview');
</script>
<!-- End Google Analytics -->
