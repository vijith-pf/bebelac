<header>
	<nav class="navbar navbar-expand-lg bg-primary <?php if(!is_front_page()): echo 'header-inner'; endif ?>">
	  <div class="container">
	    <a class="navbar-brand d-lg-none" href="<?php echo home_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/bebelac-logo.svg" alt="<?php bloginfo('name'); ?>" /></a>
	    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggle" aria-controls="navbarToggle" aria-expanded="false" aria-label="Toggle navigation">
	      <span class="navbar-toggler-icon"></span>
	    </button>
	    <div class="collapse navbar-collapse justify-content-between" id="navbarToggle">
	      <ul class="navbar-nav">
	        <li class="nav-item active">
	          <a class="nav-link" href="<?php echo home_url(); ?>/iron-test">Iron Test</a>
	        </li>
	        <li class="nav-item">
	          <a class="nav-link" href="#">Inhibitors and Enhancers</a>
	        </li>
	      </ul>
	      <a class="navbar-brand d-none d-lg-block" href="<?php echo home_url(); ?>">
	        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/bebelac-logo.svg" alt="<?php bloginfo('name'); ?>" />
	      </a>
	      <ul class="navbar-nav">
	        <li class="nav-item">
	          <a class="nav-link" href="#">Products</a>
	        </li>
	        <li class="nav-item">
	          <a class="nav-link" href="#">About</a>
	        </li>
	        <li class="nav-item">
	          <a class="nav-link" href="#">عربى</a>
	        </li>
	      </ul>
	    </div>
	  </div>
	</nav>
</header>

<?php
/*
  if (has_nav_menu('primary_navigation')) :
	wp_nav_menu(array('theme_location' => 'primary_navigation', 'menu_class' => 'nav navbar-nav navbar-right'));
  endif;
*/
?>