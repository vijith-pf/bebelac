<?php // get_template_part('templates/page', 'header'); ?>
<?php // get_template_part('templates/content', 'page'); ?>


<section class="hero-wrap clip-mask">
<div class="banner-wrap">
  <div class="hero-img">
    <div class="container">
      <div class="overlay">
        <div class="banner-txt">
          <h1>IN JORDAN, 19.1% OF<br/> TODDLERS SUFFER FROM <strong>IRON DEFICIENCY</strong></h1>
          <h2>Does your child get enough iron? </h2>
          <a href="<?php echo home_url(); ?>/iron-test" class="btn">TAKE THE IRON TEST <i class="icon icon-arrow"></i></a>
        </div>
        <!--
        <div class="baby-img-wrap">
          <img src="<?php // echo get_template_directory_uri(); ?>/contents/banner-baby.png" alt="" />
        </div>
        -->
      </div>
    </div>
  </div>
</div>
<!--
<svg class="clip-svg" width="0" height="0">
  <defs>
    <clipPath id="maskRect1" transform="scale(1)" clipPathUnits="objectBoundingBox">
      <path d="M0,0 1,0 1,0.9 C 1,0.9, 0.77,1, 0.5,1 0.23,1, 0,0.9,0,0.9z"/>-->
      <!--<path d="M414,30h1440v734c-245.4-33.3-487.2-50-725.4-50S652.2,730.7,414,764z" />-->
      <!--
    </clipPath>
  </defs>
</svg>
-->
</section>

<section class="why-choose" id="thing">
<div class="container">
  <div class="choose-wrap">
    <div class="info">
      <div class="title">
        <h2>Why choose<br/> Bebelac Junior 3?</h2>
      </div>
      <div class="info-flow">
        <div class="bebelac-bottle">
          <div class="img-wrap" data-mh="img-wrap">
            <img src="<?php echo get_template_directory_uri(); ?>/contents/bebebot.png" alt="" />
          </div>
          <h3>2 FEEDS OF BEBELAC JUNIOR 3<br/>(500 ml)</h3>
        </div>
        <div class="diagram-holder">
          <div class="img-wrap" data-mh="img-wrap">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/dialy.svg" alt="" />
          </div>
          <h3>DAILY IRON INTAKE</h3>
        </div>
      </div>
    </div>
    <div class="diagram">
      <div class="chart-wrap">
        <div class="chart-animation chart-vitamin-d" data-percent="70"><span></span></div>
        <h3>Vitamin D</h3>
      </div>

      <div class="chart-wrap">
        <div class="chart-animation chart-calcium" data-percent="86"><span></span></div>
        <h3>Calcium</h3>
      </div>

      <div class="chart-wrap">
        <div class="chart-animation chart-vitamin-c" data-percent="100%"><span></span></div>
        <h3>Vitamic C</h3>
      </div>
    </div>
  </div>
  <div class="btn-wrap center">
    <a href="#" class="btn btn-md btn-primary">Learn More</a>
  </div>
</div>
</section>

<section class="child-development">
<div class="container">
  <div class="development-inner">
    <div class="title">
      <h2>Iron is essential to your<br/>child's development</h2>
    </div>
    <div class="row titles-wrap">
      <div class="col-md-4">
        <div class="title">
          <h3>Growth</h3>
        </div>
      </div>
      <div class="col-md-4">
        <div class="title">
          <h3>Energy</h3>
        </div>
      </div>
      <div class="col-md-4">
        <div class="title">
          <h3>Immunity</h3>
        </div>
      </div>
    </div>
  </div>
</div>
</section>